'use strict';

const {
	expect
} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/bookings');

const server = request(createServer());

//Delete existing databases to do corrects tests
describe('Booking api', function () {
	before(async function () {
		await database.sequelize.query('DELETE from BOOKINGS');
		const {
			Bookings
		} = database;
		const promises = fixtures.map(booking => Bookings.create(booking));
		await Promise.all(promises);
	});

	//GET REFERENCE
	describe('GET /api/v1/bookings/:booking_reference', function () {
		it("La reference donnée n'existe pas alors on reçoit un 404", async () => {
			await server.get('/api/v1/bookings/je-n-existe-pas-2')
				.expect(404);
		});

		it("La reference donnée existe donc on reçoit un 200 avec un booking", async () => {
			const {
				body: booking
			} = await server.get('/api/v1/bookings/booking1')
				.expect(200);

			expect(booking.booking_reference).to.equal('booking1');
			expect(booking.user_email).to.equal('test1@gmail.com');
		});
	});

	describe('POST /api/v1/bookings', function () {
		it("Request send data of a booking and creates it and returns code 201", async () => {
			await server.post('/api/v1/bookings')
				.send({
					booking_reference: 'test',
					user_email: 'test',
					spectacle_reference: 'test'
				})
				.expect(201);
		});

		it("Request doesn't send data of a booking, doesn't creates booking and returns code 400", async () => {
			await server.post('/api/v1/bookings')
				.send({
					spectacle_reference: ''
				})
				.expect(400);
		});

		it("Request send data of a user that already exists and returns code 409", async () => {
			await server.post('/api/v1/bookings')
				.send({
					booking_reference: "booking1",
					user_email: "test1@gmail.com",
					spectacle_reference: "spectacle1"
				})
				.expect(409);
		});
	});

	//DELETE
	describe('DELETE /api/v1/bookings/:booking_reference', function() {
		it('Delete the given user and return code 204', async () => {
			await server.delete('/api/v1/bookings/booking1')
				.expect(204);
		});
		
		it("booking_reference doesn't exists, code 404", async () => {
			await server.delete('/api/v1/bookings/i-dont-exist')
				.expect(404);
		});
	});
});