'use strict';

const {
	expect
} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/spectacles');

const server = request(createServer());

//Delete existing databases to do corrects tests
describe('Spectacle api', function () {
	before(async function () {
		await database.sequelize.query('DELETE from SPECTACLES');
		const {
			Spectacles
		} = database;
		const promises = fixtures.map(spectacle => Spectacles.create(spectacle));
		await Promise.all(promises);
	});
	

	//Return all the spectacles
	describe('GET /api/v1/spectacles', function () {
		it('Recover all spectacles', async () => {
			const {
				body: spectacles
			} = await server
				.get('/api/v1/spectacles')
				.set('Accept', 'application/json')
				.expect(200);

			expect(spectacles).to.be.an('array');
			expect(spectacles.length).to.equal(3);
		});

		it('Filtering spectacles', async () => {
			const {
				body: spectacles
			} = await server
				.get('/api/v1/spectacles')
				.query({
					"name": "Name3",
				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(spectacles).to.be.an('array');
			expect(spectacles.length).to.equal(1);
			expect(spectacles[0].city).to.equal('City3')
		});
	});


	//POST
	describe('POST /api/v1/spectacles', function () {
		it("La requete envoie tous les données d'un spectacle, le spectacle est créé et on reçoit un 201", async () => {
			await server.post('/api/v1/spectacles')
				.send({
					"reference": "reference-spectacle-4",
					"name": "Name4",
					"artist": "Artist4",
					"date": "2019-01-04",
					"price": 200,
					"placesLeft": 30,
					"city": "City4"
				})
				.expect(201);
		});

		it("La requete envoie n'evoie pas tous les données d'un spectacle, on reçoit un 400", async () => {
			await server.post('/api/v1/spectacles')
				.send({
					"reference": "reference-spectacle-4",
					"name": "Name4",
					"artist": "Artist4",
				})
				.expect(400);
		});

		it("La requete envoie un spectacle qui existe déjà, on reçoit un 409", async () => {
			await server.post('/api/v1/spectacles')
				.send({
					"reference": "reference-spectacle-1",
					"name": "Name1",
					"artist": "Artist1",
					"date": "2019-01-01",
					"price": 200,
					"placesLeft": 30,
					"city": "City1"
				})
				.expect(409);
		});
	});


	//GET REFERENCE
	describe('GET /api/v1/spectacles/:reference', function () {
		it("La reference donnée n'existe pas alors on reçoit un 404", async () => {
			await server.get('/api/v1/spectacles/je-n-existe-pas-2')
				.expect(404);
		});

		it("La reference donnée existe donc on reçoit un 200 avec un spectacle", async () => {
			const {
				body: spectacle
			} = await server.get('/api/v1/spectacles/reference-spectacle-1')
				.expect(200);

			expect(spectacle.reference).to.equal('reference-spectacle-1');
			expect(spectacle.name).to.equal('Name1');
		});
	});


	//PUT
	describe('PUT /api/v1/spectacles/:reference', function () {
		it("La référence donnée n'existe pas, on reçoit donc un 404", async () => {
			await server.put('/api/v1/spectacles/reference-qui-nexiste-pas')
				.send({
					city: 'Caen'
				})
				.expect(404);
		});

		it('Il manque des données. On reçoit un 400.', async () => {
			await server
				.put('/api/v1/spectacles/reference-spectacle-1')
				.set('Accept', 'application/json')
				.send({
					city: '',
				})
				.expect(400);
		});

		it('Tout est OK, on reçoit un 200.', async () => {
			await server
				.put('/api/v1/spectacles/reference-spectacle-3')
				.set('Accept', 'application/json')
				.send({
					artist: "Jean-Jacques Goldman",
					city:"Paris",
					placesLeft: 0
				})
				.expect(200);
		});
	});

	//DELETE
	describe('DELETE /api/v1/spectacles/:reference', function() {
		it('Delete the given user and return code 204', async () => {
			await server.delete('/api/v1/spectacles/reference-spectacle-2')
				.expect(204);
		});
		
		it("reference doesn't exists, code 404", async () => {
			await server.delete('/api/v1/spectacles/i-dont-exist')
				.expect(404);
		});
	});
});