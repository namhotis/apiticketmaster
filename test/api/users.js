'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/users');

const server = request(createServer());

describe('User api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from USERS');
		const {Users} = database;
		const promises = fixtures.map(user => Users.create(user));
		await Promise.all(promises);
	});

	describe('GET /api/v1/users', function() {
		it('Recover all users and return code 200', async () => {
			const {body: users} = await server
				.get('/api/v1/users')
				.set('Accept', 'application/json')
				.expect(200);
			expect(users).to.be.an('array');
			expect(users.length).to.equal(3);
		});
	});

	describe('POST /api/v1/users', function () {
		it("Request send data of a user and creates it and returns code 201", async () => {
			await server.post('/api/v1/users')
				.send({
					fullname: 'Jane Doe',
					age: '26',
					email: 'jane@gmail.com'
				})
				.expect(201);
		});

		it("Request doesn't send data of a user, doesn't creates user and returns code 400", async () => {
			await server.post('/api/v1/users')
				.send({
					fullname: 'John Doe'
				})
				.expect(400);
		});

		it("Request send data of a user that already exists and returns code 409", async () => {
			await server.post('/api/v1/users')
				.send({
					fullname: 'John Doe',
					age: '34',
					email: 'test@gmail.com'
				})
				.expect(409);
		});
	});

	describe('GET /api/v1/users/:email', function() {
		it("Email doesn't exists, code 404", async () => {
			await server.get('/api/v1/users/i-dont-exist')
				.expect(404);
		});

		it("Email exists, code 200", async () => {
			await server.get('/api/v1/users/test@gmail.com')
				.expect(200);
		});
	});

	describe('PUT /api/v1/users/:email', function() {
		it("Email doesn't exists, code 404", async () => {
			await server.put('/api/v1/users/i-dont-exist')
				.send({
					fullname: 'Johnny',
					age: '25'
				})
				.expect(404);
		});

		it('Form data are missing, code 400', async () => {
			await server
				.put('/api/v1/users/test@gmail.com')
				.set('Accept', 'application/json')
				.send({
					fullname: '',
				})
				.expect(400);
		});

		it('Form data are okay, code 200', async () => {
			await server
				.put('/api/v1/users/test@gmail.com')
				.set('Accept', 'application/json')
				.send({
					fullname: 'Johnny',
					age: '25',
				})
				.expect(200);
		});
	});

	describe('DELETE /api/v1/users/:email', function() {
		it('Delete the given user and return code 204', async () => {
			await server.delete('/api/v1/users/test@gmail.com')
				.expect(204);
		});
		
		it("Email doesn't exists, code 404", async () => {
			await server.delete('/api/v1/users/i-dont-exist')
				.expect(404);
		});
	});
	
});