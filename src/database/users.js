'use strict';

const Users = (sequelize, DataTypes) => {
	return sequelize.define('Users', {
		fullname: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing fullname'}}
		},
		age: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing age'}}
		},
		email: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing email'}},
			allowNull: false
		}
	});
};

module.exports = Users;
