'use strict';

const Bookings = (sequelize, DataTypes) => {
	return sequelize.define('Bookings', {
		booking_reference: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing reference'}}
		},
		user_email: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing user'}},
			allowNull: false
		},
		spectacle_reference: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing spectacle'}},
            allowNull: false,
		}
	});
};

module.exports = Bookings;