'use strict';

const Spectacles = (sequelize, DataTypes) => {
	return sequelize.define('Spectacles', {
		reference: {
			type: DataTypes.STRING,
			primaryKey: true,
			validate: {notEmpty: {msg: '-> Missing reference'}}
		},
		name: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing origin'}},
			allowNull: false
		},
		artist: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing destination'}},
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			validate: {notEmpty: {msg: '-> Missing date'}},
			allowNull: false
		},
		price: {
			type: DataTypes.DECIMAL(10, 2),
			validate: {notEmpty: {msg: '-> Missing price'}},
			allowNull: false
		},
		placesLeft: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing placesLeft'}},
			allowNull: false
		},
		city: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing company'}},
			allowNull: false
		}
	});
};

module.exports = Spectacles;
