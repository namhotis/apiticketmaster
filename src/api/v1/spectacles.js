const express = require('express');
const request = require('request-promise');
const router = express.Router();

/* GET spectacles listing. */
router.get('/', async (req, res, next) => {
	try {

		const {
			name,
			artist,
			date,
			city
		} = req.query;
		const filter = {
			where: {}
		};
		if (name) filter.where.name = name;
		if (artist) filter.where.artist = artist;
		if (date) filter.where.date = date;
		if (city) filter.where.city = city;

		const {
			Spectacles
		} = req.db;
		const spectacles = await Spectacles.findAll(filter);

		res.send(spectacles);
	} catch (err) {
		next(err);
	}
});

router.post('/', async (req, res) => {
	try {
		const body = req.body;
		if (body.name && body.artist && body.date && body.price && body.placesLeft && body.city) {
			// Insert dans la bdd
			const {
				Spectacles
			} = req.db;
			const spectacle = await Spectacles.create(body);
			return res.status(201).send(spectacle);
		} else {
			return res.status(400).send({
				message: 'Missing data'
			});
		}
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({
				message: 'Ce spectacle existe déjà'
			})
		}
	}

});

router.get('/:reference', async (req, res, next) => {
	try {

		const {
			reference
		} = req.params;
		const filter = {
			where: {}
		};
		if (reference) filter.where.reference = reference;

		const {
			Spectacles
		} = req.db;
		const spectacles = await Spectacles.findOne(filter);

		if (spectacles) {
			return res.status(200).send(spectacles);
		} else {
			return res.status(404)
				.send({
					message: `Reference ${reference} not found`
				});
		}
	} catch (err) {
		next(err);
	}
});

router.put('/:reference', async (req, res) => {
	const body = req.body;
	const reference = req.params.reference;
	const {
		Spectacles
	} = req.db;
	const spectacle = await Spectacles.findOne({
		where: {
			reference: reference
		}
	});
	if (spectacle) {
		if (body.city && body.artist) {
			spectacle.update({
				city: body.city,
				artist: body.artist
			});

			return res.status(200).send(spectacle);
		} else {

			return res.status(400).send({
				message: 'Missing data'
			});
		}
	} else {
		return res.status(404)
			.send({
				message: `Reference ${reference} not found`
			});
	}
});

// delete 1 spectacle
router.delete('/:reference', async (req, res) => {
	const reference = req.params.reference;
	const {Spectacles} = req.db;
	const spectacle = await Spectacles.findOne({ where: {reference: reference} });
	if (spectacle) {
		Spectacles.destroy({where: {reference: reference}})
		.then(result => {
            res.status(204).json(result);
        });
	} else {
		return res.status(404)
			.send({message: `spectacle with reference: ${reference} not found`});
	}
});


module.exports = router;