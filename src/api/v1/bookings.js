const express = require('express');
const request = require('request-promise');
const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const body = req.body;
        if (body.booking_reference && body.user_email && body.spectacle_reference) {
            // Insert dans la bdd
            const {
                Bookings
            } = req.db;
            const booking = await Bookings.create(body);
            return res.status(201).send(booking);
        } else {
            return res.status(400).send({
                message: 'Missing data'
            });
        }
    } catch (err) {
        if (err.name === 'SequelizeUniqueConstraintError') {
            return res.status(409).send({
                message: 'Ce booking existe déjà'
            })
        }
    }

});

router.get('/:booking_reference', async (req, res, next) => {
    const booking_reference = req.params.booking_reference;
    const {Bookings} = req.db;
	const booking = await Bookings.findOne({ where: {booking_reference: booking_reference} });
	if (booking) {
		return res.status(200).send(booking);
	} else {
		return res.status(404)
			.send({message: `booking with booking_reference: ${booking_reference} not found`});
	}
});

router.get('email/:user_email', async (req, res, next) => {
    const user_email = req.params.user_email;
	const {Bookings} = req.db;
    const bookings = await Bookings.findAll({ where: {user_email: user_email} });
    
	if (bookings) {
		return res.status(200).send(bookings);
	} else {
		return res.status(404)
			.send({message: `booking with user_email: ${user_email} not found`});
	}
});

// delete 1 booking
router.delete('/:booking_reference', async (req, res) => {
	const booking_reference = req.params.booking_reference;
	const {Bookings} = req.db;
	const booking = await Bookings.findOne({ where: {booking_reference: booking_reference} });
	if (booking) {
		Bookings.destroy({where: {booking_reference: booking_reference}})
		.then(result => {
            res.status(204).json(result);
        });
	} else {
		return res.status(404)
			.send({message: `booking with booking_reference: ${booking_reference} not found`});
	}
});


module.exports = router;