const express = require('express');
const router = express.Router();

// get all users
router.get('/', async (req, res) => {
	const {Users} = req.db;
	const users = await Users.findAll();
	if (users) {
		return res.status(200).send(users);
	}
});

// Create new user
router.post('/', async(req, res) => {
	try {
		// Vérifier que il y a un nom, prenom, email
		const body = req.body;
		if (body.fullname && body.age && body.email) {
			// Insert dans la bdd
			const {Users} = req.db;
			const user = await Users.create(body);
			return res.status(201).send(user);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'User already exists'})
		}
	}
	
});

// get 1 user
router.get('/:email', async (req, res) => {
	const email = req.params.email;
	const {Users} = req.db;
	const user = await Users.findOne({ where: {email: email} });
	if (user) {
		return res.status(200).send(user);
	} else {
		return res.status(404)
			.send({message: `User with email: ${email} not found`});
	}
});

// delete 1 user
router.delete('/:email', async (req, res) => {
	const email = req.params.email;
	const {Users} = req.db;
	const user = await Users.findOne({ where: {email: email} });
	if (user) {
		Users.destroy({where: {email: email}})
		.then(result => {
            res.status(204).json(result);
        });
	} else {
		return res.status(404)
			.send({message: `User with email: ${email} not found`});
	}
});

// Update user
router.put('/:email', async(req, res) => {
	const body = req.body;
	const email = req.params.email;
	const {Users} = req.db;
	const user = await Users.findOne({ where: {email: email} });
	if (user) {
		if (body.fullname && body.age){
			user.update({
				fullname: body.fullname,
				age: body.age
			});
			
			return res.status(200).send(user);
		} else {

			return res.status(400).send({message: 'Missing data'});
		}
	} 
	else {
		return res.status(404)
			.send({message: `Reference ${email} not found`});
	}	
});

module.exports = router;
